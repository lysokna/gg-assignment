//
//  GGBookTableViewController.m
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "GGBookTableViewController.h"
#import "GGBookDetailViewController.h"
#import "MDSnackbar.h"
@interface GGBookTableViewController ()

@property(strong,nonatomic) NSMutableArray *books;

@end

@implementation GGBookTableViewController{
    NSInteger currentPage;
    Reachability *reacher;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.books = [NSMutableArray array];
    currentPage = 0;
    reacher = [Reachability reachabilityForInternetConnection];
    [self setupRefreshControl];
    [self.refreshControl beginRefreshing];
    [self fetchFirstPage];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)setupLoadMoreView{
    UIView *footerView = [[UIView alloc] init];
    CGRect frame = self.tableView.frame;
    frame.size.height = 40.0;
    footerView.frame = frame;
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.center = footerView.center;
    [indicator startAnimating];
    [footerView addSubview:indicator];
    self.tableView.tableFooterView = footerView;
}

-(void)setupRefreshControl{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(fetchFirstPage) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)fetchFirstPage{
    currentPage = 0;
    [self fetchBooksWithPage:0];
}

-(void)fetchBooksWithPage:(NSInteger)page{
    [[GGDataManager sharedManager] fetchBooksWithPageNumber:page andCompleteBlock:^(NSArray * _Nullable books, NSError * _Nullable error) {
        if (books && !error) {
            if (books.count == 0) {
                self.tableView.tableFooterView = nil;
            }else{
                // prevent appending data on reload
                if (page==0) {
                    [self setupLoadMoreView];
                    [self.books removeAllObjects];
                }
                [self.books addObjectsFromArray:books];
                [self.refreshControl endRefreshing];
                [self.tableView reloadData];
            }

        }else{
            MDSnackbar *snackbar = [[MDSnackbar alloc] initWithText:[error localizedDescription] actionTitle:@"DISMISS" duration:15];
            [snackbar setActionTitleColor:[UIColor whiteColor]];
            [snackbar setBackgroundColor:AppColor];
            [snackbar show];
            [snackbar setMultiline:YES];
            [self.refreshControl endRefreshing];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.books.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:BookTableViewCellIdentifier forIndexPath:indexPath];
    GGBook *book = [self.books objectAtIndex:indexPath.row];
    [cell.textLabel setText:book.title];
    if (indexPath.row == self.books.count-1) {
        [self fetchBooksWithPage:currentPage++];
    }
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        GGBookDetailViewController *detail = (GGBookDetailViewController*)segue.destinationViewController;
        detail.book = self.books[indexPath.row];
    }
}


@end
