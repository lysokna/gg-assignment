//
//  GGDataManager.h
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GGBook.h"

//typedef void (^GGBookListResultBLock) (NSArray *nullable books, NSError *nullable error);
/**
 *  GGDataManager class is created to manage data with REST Api.
 */
@interface GGDataManager : NSObject

/**
 *  Singleton object for GGDataManager to use globally
 *
 *  @return GGDataManager object
 */

+(nullable GGDataManager*)sharedManager;


/**
 *  Fetch book list from API
 *
 *  @param page          page number for pagination list
 *  @param completeBlock result block for book list
 */

-(void)fetchBooksWithPageNumber:(NSInteger)page andCompleteBlock:(nonnull void (^)(NSArray *_Nullable books,NSError *_Nullable error))completeBlock;

/**
 *  Fetch Book Detail for specific book
 *
 *  @param book          GGBook object to get detail
 *  @param completeBlock result block for book object
 */

-(void)fetchBookDetailForBook:(nonnull GGBook*)book withCompleteBlock:(nonnull void (^)(Boolean succeed,NSError *_Nullable error))completeBlock;

/**
 *  Download image with specific url asynchronously and cache it.
 *
 *  @param imageUrl      URL of image to be downloaded
 *  @param completeBlock result block for downloaded image
 */

-(void)downloadImageWithUrl:(nonnull NSString*)imageUrl andCompleteBlock:(nonnull void(^)(UIImage *_Nullable image,NSError* _Nullable error))completeBlock;

@end
