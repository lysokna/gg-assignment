//
//  UIColor+HexColor.h
//  gg-assignment
//
//  Created by SOKNA on 10/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+(nonnull UIColor*)colorWithHex:(nonnull NSString*)hexString;

@end
