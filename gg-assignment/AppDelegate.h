//
//  AppDelegate.h
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

