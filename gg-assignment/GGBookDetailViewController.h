//
//  GGBookDetailViewController.h
//  gg-assignment
//
//  Created by Sokna Ly on 10/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability/Reachability.h"
#import "MDSnackbar.h"
@interface GGBookDetailViewController : UIViewController

@property(strong,nonatomic) GGBook *book;

@end
