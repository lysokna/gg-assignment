//
//  GGDataManager.m
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "GGDataManager.h"
#import <AFNetworking.h>
@implementation GGDataManager{
    AFHTTPRequestOperationManager *httpManager;
}

+(GGDataManager *)sharedManager{
    static GGDataManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

-(instancetype)init{
    if (self=[super init]) {
        httpManager = [AFHTTPRequestOperationManager manager];
        BOOL isDirectory = YES;
        NSString *imageCachePath = [self cachePathForFileName:@"Images"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:imageCachePath isDirectory:&isDirectory]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:imageCachePath withIntermediateDirectories:NO attributes:nil error:nil];
        }
    }
    return self;
}

-(void)fetchBooksWithPageNumber:(NSInteger)page andCompleteBlock:(void (^)(NSArray * _Nullable, NSError * _Nullable))completeBlock{
    NSInteger offset = page * BOOK_LIMIT;
    httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [httpManager GET:[NSString stringWithFormat:@"%@/api/v10/items",ApiEndPoint] parameters:@{kApiOffset:@(offset),kApiCount:@(BOOK_LIMIT)} success:^(AFHTTPRequestOperation * _Nonnull operation, NSArray *  _Nonnull responseObject) {
        NSMutableArray *books = [NSMutableArray array];
        for (int i=0; i<responseObject.count; i++) {
            GGBook *book = [[GGBook alloc] initWithDictionary:[responseObject objectAtIndex:i]];
            [books addObject:book];
        }
        completeBlock(books,nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        completeBlock(nil,error);
    }];
}

-(void)fetchBookDetailForBook:(GGBook *)book withCompleteBlock:(void (^)(Boolean, NSError * _Nullable))completeBlock{
    httpManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [httpManager GET:[NSString stringWithFormat:@"%@%@",ApiEndPoint,book.link] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [book setBookDetailWithDictionary:responseObject];
        completeBlock(true,nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        completeBlock(false,error);
    }];
}

-(void)downloadImageWithUrl:(NSString *)imageUrl andCompleteBlock:(void (^)(UIImage * _Nullable, NSError * _Nullable))completeBlock{
    NSString *imagePath = [self cachePathForFileName:[NSString stringWithFormat:@"Images/%@",[self fileNameForImageUrl:imageUrl]]];
    if ([self imageWithUrlExists:imageUrl]) {
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        completeBlock(image,nil);
    }else{
        httpManager.responseSerializer = [AFImageResponseSerializer serializer];
        [httpManager GET:imageUrl parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            UIImage *responseImage = (UIImage*)responseObject;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData *imageData = UIImageJPEGRepresentation(responseImage, 1);
                [imageData writeToFile:imagePath atomically:YES];
            });
            completeBlock(responseImage,nil);
        } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
            completeBlock(nil,error);
        }];
    }
}

/**
 *  Check Existing Image in cache foldre
 *
 *  @param imageUrl url of image to be checked
 *
 *  @return YES for exists else NO
 */

-(Boolean)imageWithUrlExists:(NSString*)imageUrl{
    NSString *imageName = [self fileNameForImageUrl:imageUrl];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self cachePathForFileName:[NSString stringWithFormat:@"Images/%@",imageName]]]){
        return YES;
    }
    return NO;
}

/**
 *  Get File name from URL of Image
 *
 *  @param imageUrl url of image to get
 *
 *  @return filename of image
 */

-(NSString*)fileNameForImageUrl:(NSString*)imageUrl{
    NSArray *components = [imageUrl componentsSeparatedByString:@"/"];
    return components[4];
}

/**
 *  Get Cache Path of filename
 *
 *  @param filename filename to get path
 *
 *  @return path of file
 */

-(NSString*)cachePathForFileName:(NSString*)filename{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSAllDomainsMask, YES);
    NSString *filePath = [paths[0] stringByAppendingPathComponent:filename];
    return filePath;
}



@end
