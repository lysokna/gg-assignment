//
//  GGConstants.h
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#define BOOK_LIMIT 25
#define AppColor [UIColor colorWithHex:@"#0277BD"]
@interface GGConstants : NSObject

extern NSString *const kBookId;
extern NSString *const kBookTitle;
extern NSString *const kBookLink;
extern NSString *const kBookImageUrl;
extern NSString *const kBookAuthor;
extern NSString *const kBookPrice;
extern NSString *const ApiEndPoint;
extern NSString *const kApiOffset;
extern NSString *const kApiCount;
extern NSString *const BookTableViewCellIdentifier;


@end
