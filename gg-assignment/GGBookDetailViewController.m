//
//  GGBookDetailViewController.m
//  gg-assignment
//
//  Created by Sokna Ly on 10/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "GGBookDetailViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
@interface GGBookDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *bookImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@end

@implementation GGBookDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:self.book.title];
    [self.titleLabel setText:@""];
    [self.authorLabel setText:@""];
    [self.priceLabel setText:@""];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:@"Loading..."];
    [[GGDataManager sharedManager] fetchBookDetailForBook:self.book withCompleteBlock:^(Boolean succeed, NSError * _Nullable error) {
        if (succeed && !error) {
            [[GGDataManager sharedManager] downloadImageWithUrl:self.book.imageUrl andCompleteBlock:^(UIImage * _Nullable image, NSError * _Nullable error) {
                if (image) {
                    [self.bookImageView setImage:image];
                }
                [hud hide:YES];
            }];
            [self.titleLabel setText:self.book.title];
            [self.authorLabel setText:self.book.author];
            [self.priceLabel setText:[NSString stringWithFormat:@"$%.2f",self.book.price]];
        }else{
            [hud hide:YES];
            MDSnackbar *snackbar = [[MDSnackbar alloc] initWithText:[error localizedDescription] actionTitle:@"DISMISS" duration:15];
            [snackbar setActionTitleColor:[UIColor whiteColor]];
            [snackbar setBackgroundColor:AppColor];
            [snackbar show];
            [snackbar setMultiline:YES];
        }

    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
