//
//  GGBook.m
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "GGBook.h"



@implementation GGBook



-(instancetype)initWithDictionary:(NSDictionary *)dict{
    if (self=[super init]) {
        [self setObjectId:[dict valueForKey:kBookId]];
        [self setTitle:[dict valueForKey:kBookTitle]];
        [self setLink:[dict valueForKey:kBookLink]];
    }
    return self;
}

-(void)setBookDetailWithDictionary:(NSDictionary *)detail{
    [self setAuthor:[detail valueForKey:kBookAuthor]];
    [self setImageUrl:[detail valueForKey:kBookImageUrl]];
    [self setPrice:[[detail valueForKey:kBookPrice] floatValue]];
}


-(NSString *)debugDescription{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:self.objectId forKey:kBookId];
    [dict setValue:self.title forKey:kBookTitle];
    [dict setValue:self.link forKey:kBookLink];
    if (self.imageUrl.length > 0) {
        [dict setValue:self.imageUrl forKey:kBookImageUrl];
    }
    if (self.author.length > 0) {
        [dict setValue:self.author forKey:kBookAuthor];
    }
    if (self.price) {
        [dict setValue:@(self.price) forKey:kBookPrice];
    }
    return [NSString stringWithFormat:@"<GGBook: %p>%@",self,dict];
}



@end
