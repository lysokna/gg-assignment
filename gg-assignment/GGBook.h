//
//  GGBook.h
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GGBook : NSObject

@property(strong,nonatomic) NSString *objectId;

@property(strong,nonatomic) NSString *title;

@property(strong,nonatomic) NSString *link;

@property(strong,nonatomic) NSString *imageUrl;

@property(strong,nonatomic) NSString *author;

@property(nonatomic) CGFloat price;


/**
 *  This method will create GGBook
 *
 *  @param dict response from api
 *
 *  @return instance object of GGBook
 */

-(instancetype)initWithDictionary:(NSDictionary*)dict;

/**
 *  Set detail for GGBook
 *
 *  @param detail response from item detail
 */

-(void)setBookDetailWithDictionary:(NSDictionary*)detail;

@end
