//
//  GGConstants.m
//  gg-assignment
//
//  Created by SOKNA on 10/24/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "GGConstants.h"

@implementation GGConstants

NSString *const ApiEndPoint = @"http://assignment.gae.golgek.mobi";

NSString *const kBookId = @"id";
NSString *const kBookTitle = @"title";
NSString *const kBookLink = @"link";
NSString *const kBookImageUrl = @"image";
NSString *const kBookAuthor = @"author";
NSString *const kBookPrice = @"price";

NSString *const kApiOffset = @"offset";
NSString *const kApiCount = @"count";
NSString *const BookTableViewCellIdentifier = @"BookCell";

@end
