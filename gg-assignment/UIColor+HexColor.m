//
//  UIColor+HexColor.m
//  gg-assignment
//
//  Created by SOKNA on 10/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "UIColor+HexColor.h"

@implementation UIColor (HexColor)

+(UIColor *)colorWithHex:(NSString *)hexString{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    scanner.scanLocation = 1; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
